﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientesController : ControllerBase
    {
        private IrwinContext db;

        public ClientesController(IrwinContext context){
            this.db = context;
        }

       [HttpGet]
        public ActionResult Get(string filtro)
        {
            var clientes = from cliente in db.Clientes select new {
               id =  cliente.Id,
               nombre = cliente.Nombre,
               direccion = cliente.Direccion,
               telefono = cliente.Telefono
            };

            if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));
            
            if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

               if(!string.IsNullOrEmpty(filtro))
               clientes =  clientes.Where(c => c.nombre.Contains(filtro));

            var totalRows = clientes.Count();
            return Ok(new { clientes, totalRows }) ;
        }

        

       [HttpPut]
        public ActionResult Put( Clientes clientes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(clientes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult Post(Clientes clientes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Clientes.Add(clientes);
            db.SaveChanges();

            return Ok( new { id = clientes.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var clientes = db.Clientes.Find(id);
            if (clientes == null)
            {
                return NotFound();
            }

            db.Clientes.Remove(clientes);
            db.SaveChanges();

            return Ok(clientes);
        }

    }
}