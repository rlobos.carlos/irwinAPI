using System.Linq;
using System.Data;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using IrwinAPI.Models.Requests;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using IrwinAPI.Models.DB;

namespace IrwinAPI.Controllers{

[ApiController]
[Route("[controller]")]
public class CotizacionEquiposController : ControllerBase{

    private readonly IrwinContext context;
    private readonly IMapper mapper;

    public CotizacionEquiposController(IrwinContext context, IMapper mapper){
        this.context = context;
        this.mapper = mapper;
    }

    [HttpGet("{id}")]
    public ActionResult Get(int id){

        var result = context.CotizacionEquipos
                .Where(c => c.CotizacionId == id)
                .Select(e => new {
                    e.Id,
                    e.CotizacionId,
                    e.EquipoId,
                    nombreEquipo = e.Equipos.Nombre,
                    e.Equipos.Codigo,
                    e.Equipos.Capacidad,
                    marca = e.Equipos.Marca.Nombre,
                    tipo = e.Equipos.Tipo.Nombre,
                    tecnologia = e.Equipos.Tecnologia.Nombre,
                    e.PrecioVenta,
                    precioBase = e.Equipos.Precio

                }).ToList();
        
        return Ok(result);
    }

    [HttpPost]
    public ActionResult Post(CotizacionEquipoRequest request){

        var cotizacionEquipo = mapper.Map<CotizacionEquipos>(request);

        context.CotizacionEquipos.Add(cotizacionEquipo);
        context.SaveChanges();

        return Ok(cotizacionEquipo.Id);
    }

    [HttpPut]
    public ActionResult Put(CotizacionEquipoRequest request){

        var cotizacionEquipo = mapper.Map<CotizacionEquipos>(request);

        context.Entry(cotizacionEquipo).State = EntityState.Modified;
        context.SaveChanges();

        return NoContent();
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id){

        var cotizacionEquipo = context.CotizacionEquipos.Find(id);

        if(cotizacionEquipo == null)
            return NotFound();

        context.CotizacionEquipos.Remove(cotizacionEquipo);
        context.SaveChanges();

        return Ok(cotizacionEquipo);
    }
    
}

}