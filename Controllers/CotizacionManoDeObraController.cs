using System.Linq;
using System.Data;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using IrwinAPI.Models.Requests;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using IrwinAPI.Models.DB;

namespace IrwinAPI.Controllers{

[ApiController]
[Route("[controller]")]
public class CotizacionManoDeObraController : ControllerBase{

    private readonly IrwinContext context;
    private readonly IMapper mapper;

    public CotizacionManoDeObraController(IrwinContext context, IMapper mapper){
        this.context = context;
        this.mapper = mapper;
    }

    [HttpGet("{id}")]
    public ActionResult Get(int id){

        var result = context.CotizacionManoDeObra
                .Where(c => c.CotizacionId == id)
                .Select(m => new {
                    m.Id,
                    m.CotizacionId,
                    m.ManoDeObraId,
                    descripcion = m.ManoDeObra.Descripcion,
                    m.ManoDeObra.Codigo,
                    m.ManoDeObra.Precio
                }).ToList();
        
        return Ok(result);
    }

    [HttpPost]
    public ActionResult Post(CotizacionManoDeObraRequest request){

        var cotizacionManoDeObra = mapper.Map<CotizacionManoDeObra>(request);

        context.CotizacionManoDeObra.Add(cotizacionManoDeObra);
        context.SaveChanges();

        return Ok(cotizacionManoDeObra.Id);
    }

    [HttpPut]
    public ActionResult Put(CotizacionManoDeObraRequest request){

        var cotizacionManoDeObra = mapper.Map<CotizacionManoDeObra>(request);

        context.Entry(cotizacionManoDeObra).State = EntityState.Modified;
        context.SaveChanges();

        return NoContent();
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id){

        var cotizacionManoDeObra = context.CotizacionManoDeObra.Find(id);

        if(cotizacionManoDeObra == null)
            return NotFound();

        context.CotizacionManoDeObra.Remove(cotizacionManoDeObra);
        context.SaveChanges();

        return Ok(cotizacionManoDeObra);
    }
    
}

}