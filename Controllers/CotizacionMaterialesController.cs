using System.Linq;
using System.Data;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using IrwinAPI.Models.Requests;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using IrwinAPI.Models.DB;

namespace IrwinAPI.Controllers{

[ApiController]
[Route("[controller]")]
public class CotizacionMaterialesController : ControllerBase{

    private readonly IrwinContext context;
    private readonly IMapper mapper;

    public CotizacionMaterialesController(IrwinContext context, IMapper mapper){
        this.context = context;
        this.mapper = mapper;
    }

    [HttpGet("{id}")]
    public ActionResult Get(int id){

        var result = context.CotizacionMateriales
                .Where(c => c.CotizacionId == id)
                .Select(m => new {
                    m.Id,
                    m.CotizacionId,
                    m.MaterialId,
                    nombre = m.Materiales.Nombre,
                    m.Materiales.Codigo,
                    m.PrecioVenta,
                    precioBase = m.Materiales.PrecioMaterialProveedor.Max(mp => mp.Precio)

                }).ToList();
        
        return Ok(result);
    }

    [HttpPost]
    public ActionResult Post(CotizacionMaterialRequest request){

        var cotizacionMaterial = mapper.Map<CotizacionMateriales>(request);

        context.CotizacionMateriales.Add(cotizacionMaterial);
        context.SaveChanges();

        return Ok(cotizacionMaterial.Id);
    }

    [HttpPut]
    public ActionResult Put(CotizacionMaterialRequest request){

        var cotizacionMaterial = mapper.Map<CotizacionMateriales>(request);

        context.Entry(cotizacionMaterial).State = EntityState.Modified;
        context.SaveChanges();

        return NoContent();
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id){

        var cotizacionMaterial = context.CotizacionMateriales.Find(id);

        if(cotizacionMaterial == null)
            return NotFound();

        context.CotizacionMateriales.Remove(cotizacionMaterial);
        context.SaveChanges();

        return Ok(cotizacionMaterial);
    }
    
}

}