﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CotizacionesController : ControllerBase
    {
        private IrwinContext db;

        public CotizacionesController(IrwinContext context){
            this.db = context;
        }

        [HttpGet]
        public ActionResult Get()
        {

            var cotizaciones = db.Cotizaciones.OrderByDescending(c => c.Id)
                .Select(m => new { 
                    id = m.Id,
                    descripcion = m.Descripcion,
                    cliente = new { 
                        nombre = m.Cliente.Nombre, 
                        id =m.Cliente.Id } });
            int totalRows = db.Cotizaciones.Count();
            return Ok(new { cotizaciones, totalRows });

        }

           



      [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            Cotizaciones cotizaciones = db.Cotizaciones.Find(id);
            if (cotizaciones == null)
            {
                return NotFound();
            }

            return Ok(cotizaciones);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, Cotizaciones cotizaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cotizaciones.Id)
            {
                return BadRequest();
            }

            db.Entry(cotizaciones).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
               
            }

            return NoContent();
        }

        [HttpPost]
        public ActionResult Post(Cotizaciones cotizaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            cotizaciones.Fecha = DateTime.Now;

            db.Cotizaciones.Add(cotizaciones);
            db.SaveChanges();

            return Ok( new { id = cotizaciones.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            Cotizaciones cotizaciones = db.Cotizaciones.Find(id);
            if (cotizaciones == null)
            {
                return NotFound();
            }

            db.Cotizaciones.Remove(cotizaciones);
            db.SaveChanges();

            return Ok(cotizaciones);
        }

    }
}