﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EquiposController : ControllerBase
    {
        private IrwinContext db;

        public EquiposController(IrwinContext context){
            this.db = context;
        }

        // GET: api/Equipos
        [HttpGet]
        public ActionResult Get()
        {
            var equipos = db.Equipos
                .Select(e => new {
                    codigo = e.Codigo, 
                    nombre = e.Nombre,
                    id = e.Id, 
                    capacidad= e.Capacidad, 
                    precio= e.Precio,
                    voltaje = new {e.Voltaje.Id, e.Voltaje.Nombre},
                    gas = new { id =  e.Gas.Id, nombre = e.Gas.Nombre },
                    marca = new {id =  e.Marca.Id, nombre = e.Marca.Nombre },
                    tecnologia = new {id =  e.Tecnologia.Id, nombre =e.
                    Tecnologia.Nombre },
                    tipo = new {id =  e.Tipo.Id, nombre = e.Tipo.Nombre}
                    

                });
            int totalRecords = db.Equipos.Count();

            return Ok(new { equipos, totalRecords });
               
        }

       [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var equipos = db.Equipos.Find(id);
            if (equipos == null)
            {
                return NotFound();
            }

            return Ok(equipos);
        }

        [HttpPut]
        public ActionResult Put( Equipos equipos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

           

            db.Entry(equipos).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
               
            }

            return NoContent();
        }

        // POST: api/Equipos
       [HttpPost]
        public ActionResult Post(Equipos equipos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Equipos.Add(equipos);
            db.SaveChanges();

            return Ok(new { id = equipos.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var equipos = db.Equipos.Find(id);
            if (equipos == null)
            {
                return NotFound();
            }

            db.Equipos.Remove(equipos);
            db.SaveChanges();

            return Ok(equipos);
        }

       
    }
}