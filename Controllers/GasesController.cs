﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class GasesController : ControllerBase
    {
        private IrwinContext db;

        public GasesController(IrwinContext context){
            this.db = context;
        }

       [HttpGet]
        public ActionResult Get()
        {
            var gases = from gas in db.Gases select new { id = gas.Id, nombre = gas.Nombre };
            var totalRecords = db.Gases.Count();
            return Ok(new
            {
                 gases,
                totalRecords
            });
        }

        [HttpPut]
        public ActionResult Put( Gases gases)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(gases).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }

            return NoContent();
        }

       [HttpPost]
        public ActionResult Post(Gases gases)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Gases.Add(gases);
            db.SaveChanges();

            return Ok( new { id = gases.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var gases = db.Gases.Find(id);
            if (gases == null)
            {
                return NotFound();
            }

            db.Gases.Remove(gases);
            db.SaveChanges();

            return Ok(gases);
        }
    }
}