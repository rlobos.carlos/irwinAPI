using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using irwinAPI.Models.DB;
using IrwinAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
namespace JWTAuthenticationExample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IrwinContext context;
       
        public LoginController(IConfiguration config, IrwinContext context)
        {
            this.context = context;
            _config = config;
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login([FromBody] User login)
        {
            IActionResult response = Unauthorized();
            User user = AuthenticateUser(login);
            if (user != null)
            {
                var tokenString = GenerateJWTToken(user);
                response = Ok(new
                {
                    token = tokenString,
                });
            }
            return response;
        }
        User AuthenticateUser(User loginCredentials)
        {
           return context.Usuarios
           .Where(u => u.Usuario == loginCredentials.Usuario && u.Contraseña == loginCredentials.Contraseña)
           .Select(u => new User{
               Contraseña = u.Contraseña,
               Usuario = u.Usuario
           }).FirstOrDefault();

        }
        string GenerateJWTToken(User userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new Claim[]{
                    new Claim("rol","ADMIN")
            };
           
            var token = new JwtSecurityToken(
            issuer: _config["Jwt: Issuer"],
            audience: _config["Jwt: Audience"],
            expires: DateTime.Now.AddYears(1),
            signingCredentials: credentials,
            claims : claims
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}