using System.Linq;
using System.Threading.Tasks;
using irwinAPI.Models.DB;
using IrwinAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("/[controller]")]

    public class ManoDeObraController : ControllerBase{

        private readonly IrwinContext context;

        public ManoDeObraController(IrwinContext context){
                this.context =  context;
        }

        [HttpGet]
        public async Task<ActionResult> Get(){
            var result = await context.ManoDeObra.ToListAsync();
            return Ok(result);
        }

       

        [HttpPost]
        public async Task<ActionResult> Post(ManoDeObra manoDeObra){
           context.Add(manoDeObra);
           await context.SaveChangesAsync();
           return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> Put(ManoDeObra manoDeObra){
           context.Entry(manoDeObra).State = EntityState.Modified;
           await context.SaveChangesAsync();
           return Ok();
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id) 
           {
               context.ManoDeObra.Remove(context.ManoDeObra.Find(id));
               context.SaveChanges();
              return Ok();
           }

    }
    
}