﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MarcasController : ControllerBase
    {
        private IrwinContext db;

        public MarcasController(IrwinContext context){
            this.db = context;
        }

        // GET: api/Marcas
        [HttpGet]
        public ActionResult Get()
        {
            var marcas = from marca in db.Marcas select new
            {
                id = marca.Id,
                nombre = marca.Nombre
            };
            int totalRows = marcas.Count();
            return Ok(new { marcas, totalRows });
        }

        

        // PUT: api/Marcas/5
       [HttpPut]
        public ActionResult Put(Marcas marcas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(marcas).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
               
            }

            return NoContent();
        }

        // POST: api/Marcas
        [HttpPost]
        public ActionResult Post(Marcas marcas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Marcas.Add(marcas);
            db.SaveChanges();

            return Ok(new { id = marcas.Id });
        }

        // DELETE: api/Marcas/5
        [HttpDelete("{id}")]
        public ActionResult Deletemarcas(int id)
        {
            Marcas marcas = db.Marcas.Find(id);
            if (marcas == null)
            {
                return NotFound();
            }

            db.Marcas.Remove(marcas);
            db.SaveChanges();

            return Ok(marcas);
        }
    }
}