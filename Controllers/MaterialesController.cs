﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using IrwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MaterialesController : ControllerBase
    {
        private IrwinContext db;

        public MaterialesController(IrwinContext context ){
            this.db = context;
        }

       [HttpGet]   
       public ActionResult Get()
        {
            var materiales = db.Materiales
                .Select( m=> new {
                   id = m.Id,
                    codigo = m.Codigo,
                    nombre = m.Nombre,
                    precio = m.PrecioMaterialProveedor.Max(mp => mp.Precio),
                    unidadMedida = new {
                                        m.UnidadMedida.Id,
                                        m.UnidadMedida.Nombre,
                                        m.UnidadMedida.Abreviatura
                                    },
                    precioProveedor = m.PrecioMaterialProveedor
                                        .Select(p => new {
                                            p.Id,
                                            p.ProveedorId,
                                            p.CodigoMaterial,
                                            p.Precio
                                        })
                });
            int totalRecords = db.Materiales.Count();
            return Ok(new { materiales, totalRecords });
        }

        [HttpGet("{id}'")]
        public ActionResult Get(int id)
        {
            var materiales = db.Materiales.Find(id);
            if (materiales == null)
            {
                return NotFound();
            }

            return Ok(materiales);
        }

        [HttpPut]
        public ActionResult Put( Materiales materiales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

           

            db.Entry(materiales).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }

            return NoContent();
        }

       [HttpPost]
        public ActionResult Post(Materiales materiales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Materiales.Add(materiales);
            db.SaveChanges();

            return Ok(new { id = materiales.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var materiales = db.Materiales.Find(id);
            if (materiales == null)
            {
                return NotFound();
            }

            db.Materiales.Remove(materiales);
            db.SaveChanges();

            return Ok(materiales);
        }

        [Route("[action]")]
        [HttpPost]
        public ActionResult AgregarPrecio(PrecioMaterialProveedor precioMaterialProveedor){

            if(db.PrecioMaterialProveedor
            .Where(
                p => p.MaterialId == precioMaterialProveedor.MaterialId 
                &&  p.ProveedorId == precioMaterialProveedor.ProveedorId)
                .Count() > 0
            ) return BadRequest("Ya existe precio para el proveedor seleccionado");

            db.PrecioMaterialProveedor.Add(precioMaterialProveedor);
            db.SaveChanges();
            return Ok(new {
                precioMaterialProveedor.Id,
                precioMaterialProveedor.CodigoMaterial,
                precioMaterialProveedor.ProveedorId,
                precioMaterialProveedor.Precio,
                precioMaterialProveedor.MaterialId
            });
        }

        [Route("[action]")]
        [HttpDelete]
        public ActionResult EliminarPrecio(int id){

            db.PrecioMaterialProveedor.Remove(db.PrecioMaterialProveedor.Find(id));
            db.SaveChanges();

           return Ok();
        }

       
    }
}