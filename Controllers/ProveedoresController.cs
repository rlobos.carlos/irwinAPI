using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace IrwinAPI.Controllers{

    [Route("[controller]")]
    public class ProveedoresController : ControllerBase{
        private readonly IrwinContext context;

        public ProveedoresController(IrwinContext context){
            this.context= context;
        }
        
        [HttpGet]
        public ActionResult Get(){
            return Ok(context.Proveedores.Select(p => new{
                p.Id,
                p.Nombre
            }).ToList());
        }
        
    }
}