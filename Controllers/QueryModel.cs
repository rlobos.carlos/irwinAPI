﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IrwinAPI.Controllers
{
    public class QueryModel
    {
        public int first { get; set; }
        public int rows { get; set; }
        public string sortField { get; set; }
        public string sortOrder { get; set; }
        public string globalFilter { get; set; }
    }
}