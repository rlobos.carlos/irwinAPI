﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TecnologiasController : ControllerBase
    {
        private IrwinContext db;

        public TecnologiasController(IrwinContext context){
            this.db = context;
        }
        [HttpGet]   
        public ActionResult Get()
        {
            var tecnologias = from tecnologia in db.Tecnologias
                              select new
                              {
                                 id = tecnologia.Id,
                                 nombre = tecnologia.Nombre
                              };
            int totalRecords = tecnologias.Count();
            return Ok(new { tecnologias, totalRecords });
        }

       

       [HttpPut]
        public ActionResult Put(Tecnologias tecnologias)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            db.Entry(tecnologias).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }

            return NoContent();
        }

       [HttpPost]
        public ActionResult Post(Tecnologias tecnologias)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tecnologias.Add(tecnologias);
            db.SaveChanges();

            return Ok(new { id = tecnologias.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {  
            Tecnologias tecnologias = db.Tecnologias.Find(id);
            if (tecnologias == null)
            {
                return NotFound();
            }

            db.Tecnologias.Remove(tecnologias);
            db.SaveChanges();

            return Ok(tecnologias);
        }
    }
}