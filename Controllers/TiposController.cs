﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TiposController : ControllerBase
    {
        private IrwinContext db;

        public TiposController(IrwinContext context) {
            this.db = context;
        }

       [HttpGet]
        public ActionResult Get()
        {
            var tipos = from tipo in db.Tipos
                         select new
                         {
                            id =  tipo.Id,
                            nombre = tipo.Nombre
                         };
            int totalRows = tipos.Count();
            return Ok(new { tipos, totalRows });
        }


       [HttpPut]
        public ActionResult Put(Tipos tipos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

           

            db.Entry(tipos).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
               
            }

            return NoContent();
        }

       [HttpPost]
        public ActionResult Post(Tipos tipos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tipos.Add(tipos);
            db.SaveChanges();

            return CreatedAtAction(nameof(Get),new { id = tipos.Id });
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            Tipos tipos = db.Tipos.Find(id);
            if (tipos == null)
            {
                return NotFound();
            }

            db.Tipos.Remove(tipos);
            db.SaveChanges();

            return Ok(tipos);
        }

    
    }
}