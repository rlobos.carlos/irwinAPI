﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UnidadesMedidaController : ControllerBase
    {
        private IrwinContext db;

        public UnidadesMedidaController(IrwinContext context){
            this.db = context;
        }

        // GET: api/UnidadesMedida
        [HttpGet]
        public ActionResult Get()
        {
            var a = db.UnidadMedidas.Select(u => new {
               id =  u.Id,
               nombre = u.Nombre,
               abreviatura =  u.Abreviatura
            }).ToList();


            return Ok(new { unidades = a , totalRows = a.Count() });
        }

    
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            UnidadMedidas unidades_medida = db.UnidadMedidas.Find(id);
            if (unidades_medida == null)
            {
                return NotFound();
            }

            return Ok(unidades_medida);
        }

       [HttpPut]
        public ActionResult Put(UnidadMedidas unidades_medida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(unidades_medida).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }
            return NoContent();
        }

       [HttpPost]
        public ActionResult Post(UnidadMedidas unidades_medida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UnidadMedidas.Add(unidades_medida);
            db.SaveChanges();

            return CreatedAtAction(nameof(Get), new { id = unidades_medida.Id }, unidades_medida);
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var unidades_medida = db.UnidadMedidas.Find(id);
            if (unidades_medida == null)
            {
                return NotFound();
            }

            db.UnidadMedidas.Remove(unidades_medida);
            db.SaveChanges();

            return Ok(unidades_medida);
        }
    }
}