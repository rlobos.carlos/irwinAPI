﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using irwinAPI.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IrwinAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class VoltajesController : ControllerBase
    {
        private IrwinContext db;

        public VoltajesController(IrwinContext context){
            this.db = context;
        }

       [HttpGet]
        public ActionResult Get()
        {
            var voltajes = from voltaje in db.Voltajes select new { 
                voltaje.Id, 
                voltaje.Nombre
            };
            var totalRecords = db.Voltajes.Count();
            return Ok(new
            {
                 voltajes,
                totalRecords
            });
        }

        [HttpPut]
        public ActionResult Put( Voltajes voltaje)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(voltaje).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }

            return NoContent();
        }

       [HttpPost]
        public ActionResult Post(Voltajes voltaje)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Voltajes.Add(voltaje);
            db.SaveChanges();

            return Ok( new { id = voltaje.Id });
        }

       [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var voltajes = db.Voltajes.Find(id);
            if (voltajes == null)
            {
                return NotFound();
            }

            db.Voltajes.Remove(voltajes);
            db.SaveChanges();

            return Ok(voltajes);
        }
    }
}