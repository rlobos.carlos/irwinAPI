﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true),
                    Direccion = table.Column<string>(maxLength: 200, nullable: true),
                    Telefono = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CostosHumanos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(maxLength: 50, nullable: false),
                    Detalle = table.Column<string>(maxLength: 100, nullable: false),
                    Costo = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostosHumanos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Marcas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marcas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tecnologias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tecnologias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnidadMedidas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true),
                    Abreviatura = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnidadMedidas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Usuario = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Contraseña = table.Column<string>(unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Voltajes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Voltajes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cotizaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 2000, nullable: true),
                    ClienteId = table.Column<int>(nullable: true),
                    Fecha = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cotizaciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.Cotizaciones_dbo.Clientes_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Clientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Materiales",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(maxLength: 45, nullable: true),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true),
                    Precio = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    TipoId = table.Column<int>(nullable: true),
                    MarcaId = table.Column<int>(nullable: true),
                    UnidadMedidaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materiales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.Materiales_dbo.Marcas_MarcaId",
                        column: x => x.MarcaId,
                        principalTable: "Marcas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.Materiales_dbo.Tipos_TipoId",
                        column: x => x.TipoId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.Materiales_dbo.UnidadMedidas_UnidadMedidaId",
                        column: x => x.UnidadMedidaId,
                        principalTable: "UnidadMedidas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Equipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(maxLength: 45, nullable: true),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true),
                    Precio = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Capacidad = table.Column<string>(maxLength: 45, nullable: true),
                    TipoId = table.Column<int>(nullable: true),
                    MarcaId = table.Column<int>(nullable: true),
                    GasId = table.Column<int>(nullable: true),
                    TecnologiaId = table.Column<int>(nullable: true),
                    VoltajeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.Equipos_dbo.Gases_GasId",
                        column: x => x.GasId,
                        principalTable: "Gases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.Equipos_dbo.Marcas_MarcaId",
                        column: x => x.MarcaId,
                        principalTable: "Marcas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.Equipos_dbo.Tecnologias_TecnologiaId",
                        column: x => x.TecnologiaId,
                        principalTable: "Tecnologias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.Equipos_dbo.Tipos_TipoId",
                        column: x => x.TipoId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Equipos_Voltajes_VoltajeId",
                        column: x => x.VoltajeId,
                        principalTable: "Voltajes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 45, nullable: true),
                    CotizacionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.Areas_dbo.Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CostosHumanosDetalle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cantidad = table.Column<int>(nullable: false),
                    EsProyecto = table.Column<bool>(nullable: false),
                    CotizacionId = table.Column<int>(nullable: true),
                    AreaId = table.Column<int>(nullable: true),
                    CostoHumanoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostosHumanosDetalle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.CostosHumanosDetalle_dbo.Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.CostosHumanosDetalle_dbo.CostosHumanos_CostoHumanoId",
                        column: x => x.CostoHumanoId,
                        principalTable: "CostosHumanos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.CostosHumanosDetalle_dbo.Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EquiposAreas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PrecioEquipo = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PorcentajeGanancia = table.Column<double>(nullable: true),
                    PrecioMaterialesEquipo = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    EquipoId = table.Column<int>(nullable: true),
                    AreaId = table.Column<int>(nullable: true),
                    PrecioTotalPersonalizado = table.Column<decimal>(type: "decimal(18, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquiposAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.EquiposAreas_dbo.Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.EquiposAreas_dbo.Equipos_EquipoId",
                        column: x => x.EquipoId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MaterialEquipoAreas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cantidad = table.Column<int>(nullable: true),
                    PorcentajeGanancia = table.Column<double>(nullable: true),
                    Precio = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    MaterialId = table.Column<int>(nullable: true),
                    EquiposAreaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialEquipoAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.MaterialEquipoAreas_dbo.EquiposAreas_EquiposAreaId",
                        column: x => x.EquiposAreaId,
                        principalTable: "EquiposAreas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.MaterialEquipoAreas_dbo.Materiales_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionId",
                table: "Areas",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_AreaId",
                table: "CostosHumanosDetalle",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_CostoHumanoId",
                table: "CostosHumanosDetalle",
                column: "CostoHumanoId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionId",
                table: "CostosHumanosDetalle",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_ClienteId",
                table: "Cotizaciones",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_GasId",
                table: "Equipos",
                column: "GasId");

            migrationBuilder.CreateIndex(
                name: "IX_MarcaId",
                table: "Equipos",
                column: "MarcaId");

            migrationBuilder.CreateIndex(
                name: "IX_TecnologiaId",
                table: "Equipos",
                column: "TecnologiaId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoId",
                table: "Equipos",
                column: "TipoId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipos_VoltajeId",
                table: "Equipos",
                column: "VoltajeId");

            migrationBuilder.CreateIndex(
                name: "IX_AreaId",
                table: "EquiposAreas",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoId",
                table: "EquiposAreas",
                column: "EquipoId");

            migrationBuilder.CreateIndex(
                name: "IX_EquiposAreaId",
                table: "MaterialEquipoAreas",
                column: "EquiposAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialId",
                table: "MaterialEquipoAreas",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_MarcaId",
                table: "Materiales",
                column: "MarcaId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoId",
                table: "Materiales",
                column: "TipoId");

            migrationBuilder.CreateIndex(
                name: "IX_UnidadMedidaId",
                table: "Materiales",
                column: "UnidadMedidaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CostosHumanosDetalle");

            migrationBuilder.DropTable(
                name: "MaterialEquipoAreas");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.DropTable(
                name: "CostosHumanos");

            migrationBuilder.DropTable(
                name: "EquiposAreas");

            migrationBuilder.DropTable(
                name: "Materiales");

            migrationBuilder.DropTable(
                name: "Areas");

            migrationBuilder.DropTable(
                name: "Equipos");

            migrationBuilder.DropTable(
                name: "UnidadMedidas");

            migrationBuilder.DropTable(
                name: "Cotizaciones");

            migrationBuilder.DropTable(
                name: "Gases");

            migrationBuilder.DropTable(
                name: "Marcas");

            migrationBuilder.DropTable(
                name: "Tecnologias");

            migrationBuilder.DropTable(
                name: "Tipos");

            migrationBuilder.DropTable(
                name: "Voltajes");

            migrationBuilder.DropTable(
                name: "Clientes");
        }
    }
}
