﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class AddCascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dbo.MaterialEquipoAreas_dbo.EquiposAreas_EquiposAreaId",
                table: "MaterialEquipoAreas");

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.MaterialEquipoAreas_dbo.EquiposAreas_EquiposAreaId",
                table: "MaterialEquipoAreas",
                column: "EquiposAreaId",
                principalTable: "EquiposAreas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dbo.MaterialEquipoAreas_dbo.EquiposAreas_EquiposAreaId",
                table: "MaterialEquipoAreas");

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.MaterialEquipoAreas_dbo.EquiposAreas_EquiposAreaId",
                table: "MaterialEquipoAreas",
                column: "EquiposAreaId",
                principalTable: "EquiposAreas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
