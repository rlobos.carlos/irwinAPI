﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class CatalogoMateriales : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Materiales_dbo.Marcas_MarcaId",
                table: "Materiales");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Materiales_dbo.Tipos_TipoId",
                table: "Materiales");

            migrationBuilder.DropIndex(
                name: "IX_MarcaId",
                table: "Materiales");

            migrationBuilder.DropIndex(
                name: "IX_TipoId",
                table: "Materiales");

            migrationBuilder.DropColumn(
                name: "MarcaId",
                table: "Materiales");

            migrationBuilder.DropColumn(
                name: "Precio",
                table: "Materiales");

            migrationBuilder.DropColumn(
                name: "TipoId",
                table: "Materiales");

            migrationBuilder.CreateTable(
                name: "Proveedores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proveedores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrecioMaterialProveedor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaterialId = table.Column<int>(nullable: false),
                    ProveedorId = table.Column<int>(nullable: false),
                    Precio = table.Column<double>(nullable: false),
                    CodigoMaterial = table.Column<string>(maxLength: 100, nullable: true),
                    MaterialesId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrecioMaterialProveedor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrecioMaterialProveedor_Materiales_MaterialesId",
                        column: x => x.MaterialesId,
                        principalTable: "Materiales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrecioMaterialProveedor_Proveedores_ProveedorId",
                        column: x => x.ProveedorId,
                        principalTable: "Proveedores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrecioMaterialProveedor_MaterialesId",
                table: "PrecioMaterialProveedor",
                column: "MaterialesId");

            migrationBuilder.CreateIndex(
                name: "IX_PrecioMaterialProveedor_ProveedorId",
                table: "PrecioMaterialProveedor",
                column: "ProveedorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrecioMaterialProveedor");

            migrationBuilder.DropTable(
                name: "Proveedores");

            migrationBuilder.AddColumn<int>(
                name: "MarcaId",
                table: "Materiales",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Precio",
                table: "Materiales",
                type: "decimal(18, 2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TipoId",
                table: "Materiales",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MarcaId",
                table: "Materiales",
                column: "MarcaId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoId",
                table: "Materiales",
                column: "TipoId");

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Materiales_dbo.Marcas_MarcaId",
                table: "Materiales",
                column: "MarcaId",
                principalTable: "Marcas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Materiales_dbo.Tipos_TipoId",
                table: "Materiales",
                column: "TipoId",
                principalTable: "Tipos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
