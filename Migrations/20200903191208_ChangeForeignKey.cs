﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class ChangeForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrecioMaterialProveedor_Materiales_MaterialesId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.DropIndex(
                name: "IX_PrecioMaterialProveedor_MaterialesId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.DropColumn(
                name: "MaterialesId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.CreateIndex(
                name: "IX_PrecioMaterialProveedor_MaterialId",
                table: "PrecioMaterialProveedor",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrecioMaterialProveedor_Materiales_MaterialId",
                table: "PrecioMaterialProveedor",
                column: "MaterialId",
                principalTable: "Materiales",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrecioMaterialProveedor_Materiales_MaterialId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.DropIndex(
                name: "IX_PrecioMaterialProveedor_MaterialId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.AddColumn<int>(
                name: "MaterialesId",
                table: "PrecioMaterialProveedor",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrecioMaterialProveedor_MaterialesId",
                table: "PrecioMaterialProveedor",
                column: "MaterialesId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrecioMaterialProveedor_Materiales_MaterialesId",
                table: "PrecioMaterialProveedor",
                column: "MaterialesId",
                principalTable: "Materiales",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
