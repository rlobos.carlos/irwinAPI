﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class RefactorizacionDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Cotizaciones_dbo.Clientes_ClienteId",
                table: "Cotizaciones");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Equipos_dbo.Gases_GasId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Equipos_dbo.Marcas_MarcaId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Equipos_dbo.Tecnologias_TecnologiaId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Equipos_dbo.Tipos_TipoId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Materiales_dbo.UnidadMedidas_UnidadMedidaId",
                table: "Materiales");

            migrationBuilder.DropTable(
                name: "CostosHumanosDetalle");

            migrationBuilder.DropTable(
                name: "MaterialEquipoAreas");

            migrationBuilder.DropTable(
                name: "CostosHumanos");

            migrationBuilder.DropTable(
                name: "EquiposAreas");

            migrationBuilder.DropTable(
                name: "Areas");

            migrationBuilder.RenameIndex(
                name: "IX_UnidadMedidaId",
                table: "Materiales",
                newName: "IX_Materiales_UnidadMedidaId");

            migrationBuilder.RenameIndex(
                name: "IX_TipoId",
                table: "Equipos",
                newName: "IX_Equipos_TipoId");

            migrationBuilder.RenameIndex(
                name: "IX_TecnologiaId",
                table: "Equipos",
                newName: "IX_Equipos_TecnologiaId");

            migrationBuilder.RenameIndex(
                name: "IX_MarcaId",
                table: "Equipos",
                newName: "IX_Equipos_MarcaId");

            migrationBuilder.RenameIndex(
                name: "IX_GasId",
                table: "Equipos",
                newName: "IX_Equipos_GasId");

            migrationBuilder.RenameIndex(
                name: "IX_ClienteId",
                table: "Cotizaciones",
                newName: "IX_Cotizaciones_ClienteId");

            migrationBuilder.AlterColumn<string>(
                name: "Usuario",
                table: "Usuarios",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(20)",
                oldUnicode: false,
                oldMaxLength: 20);

            migrationBuilder.AlterColumn<string>(
                name: "Contraseña",
                table: "Usuarios",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(200)",
                oldUnicode: false,
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Fecha",
                table: "Cotizaciones",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClienteId",
                table: "Cotizaciones",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CotizacionEquipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EquipoId = table.Column<int>(nullable: false),
                    CotizacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CotizacionEquipos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CotizacionEquipos_Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CotizacionEquipos_Equipos_EquipoId",
                        column: x => x.EquipoId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CotizacionManoDeObra",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManoDeObraId = table.Column<int>(nullable: false),
                    CotizacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CotizacionManoDeObra", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CotizacionManoDeObra_Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CotizacionManoDeObra_ManoDeObra_ManoDeObraId",
                        column: x => x.ManoDeObraId,
                        principalTable: "ManoDeObra",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CotizacionMateriales",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaterialId = table.Column<int>(nullable: false),
                    CotizacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CotizacionMateriales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CotizacionMateriales_Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CotizacionMateriales_Materiales_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionEquipos_CotizacionId",
                table: "CotizacionEquipos",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionEquipos_EquipoId",
                table: "CotizacionEquipos",
                column: "EquipoId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionManoDeObra_CotizacionId",
                table: "CotizacionManoDeObra",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionManoDeObra_ManoDeObraId",
                table: "CotizacionManoDeObra",
                column: "ManoDeObraId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionMateriales_CotizacionId",
                table: "CotizacionMateriales",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionMateriales_MaterialId",
                table: "CotizacionMateriales",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cotizaciones_Clientes_ClienteId",
                table: "Cotizaciones",
                column: "ClienteId",
                principalTable: "Clientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipos_Gases_GasId",
                table: "Equipos",
                column: "GasId",
                principalTable: "Gases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipos_Marcas_MarcaId",
                table: "Equipos",
                column: "MarcaId",
                principalTable: "Marcas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipos_Tecnologias_TecnologiaId",
                table: "Equipos",
                column: "TecnologiaId",
                principalTable: "Tecnologias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipos_Tipos_TipoId",
                table: "Equipos",
                column: "TipoId",
                principalTable: "Tipos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Materiales_UnidadMedidas_UnidadMedidaId",
                table: "Materiales",
                column: "UnidadMedidaId",
                principalTable: "UnidadMedidas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cotizaciones_Clientes_ClienteId",
                table: "Cotizaciones");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipos_Gases_GasId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipos_Marcas_MarcaId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipos_Tecnologias_TecnologiaId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipos_Tipos_TipoId",
                table: "Equipos");

            migrationBuilder.DropForeignKey(
                name: "FK_Materiales_UnidadMedidas_UnidadMedidaId",
                table: "Materiales");

            migrationBuilder.DropTable(
                name: "CotizacionEquipos");

            migrationBuilder.DropTable(
                name: "CotizacionManoDeObra");

            migrationBuilder.DropTable(
                name: "CotizacionMateriales");

            migrationBuilder.RenameIndex(
                name: "IX_Materiales_UnidadMedidaId",
                table: "Materiales",
                newName: "IX_UnidadMedidaId");

            migrationBuilder.RenameIndex(
                name: "IX_Equipos_TipoId",
                table: "Equipos",
                newName: "IX_TipoId");

            migrationBuilder.RenameIndex(
                name: "IX_Equipos_TecnologiaId",
                table: "Equipos",
                newName: "IX_TecnologiaId");

            migrationBuilder.RenameIndex(
                name: "IX_Equipos_MarcaId",
                table: "Equipos",
                newName: "IX_MarcaId");

            migrationBuilder.RenameIndex(
                name: "IX_Equipos_GasId",
                table: "Equipos",
                newName: "IX_GasId");

            migrationBuilder.RenameIndex(
                name: "IX_Cotizaciones_ClienteId",
                table: "Cotizaciones",
                newName: "IX_ClienteId");

            migrationBuilder.AlterColumn<string>(
                name: "Usuario",
                table: "Usuarios",
                type: "varchar(20)",
                unicode: false,
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.AlterColumn<string>(
                name: "Contraseña",
                table: "Usuarios",
                type: "varchar(200)",
                unicode: false,
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Fecha",
                table: "Cotizaciones",
                type: "date",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<int>(
                name: "ClienteId",
                table: "Cotizaciones",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CotizacionId = table.Column<int>(type: "int", nullable: true),
                    Nombre = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.Areas_dbo.Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CostosHumanos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Costo = table.Column<double>(type: "float", nullable: false),
                    Detalle = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostosHumanos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EquiposAreas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AreaId = table.Column<int>(type: "int", nullable: true),
                    EquipoId = table.Column<int>(type: "int", nullable: true),
                    PorcentajeGanancia = table.Column<double>(type: "float", nullable: true),
                    PrecioEquipo = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PrecioMaterialesEquipo = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PrecioTotalPersonalizado = table.Column<decimal>(type: "decimal(18, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquiposAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.EquiposAreas_dbo.Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.EquiposAreas_dbo.Equipos_EquipoId",
                        column: x => x.EquipoId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CostosHumanosDetalle",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AreaId = table.Column<int>(type: "int", nullable: true),
                    Cantidad = table.Column<int>(type: "int", nullable: false),
                    CostoHumanoId = table.Column<int>(type: "int", nullable: false),
                    CotizacionId = table.Column<int>(type: "int", nullable: true),
                    EsProyecto = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostosHumanosDetalle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.CostosHumanosDetalle_dbo.Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.CostosHumanosDetalle_dbo.CostosHumanos_CostoHumanoId",
                        column: x => x.CostoHumanoId,
                        principalTable: "CostosHumanos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.CostosHumanosDetalle_dbo.Cotizaciones_CotizacionId",
                        column: x => x.CotizacionId,
                        principalTable: "Cotizaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MaterialEquipoAreas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cantidad = table.Column<int>(type: "int", nullable: true),
                    EquiposAreaId = table.Column<int>(type: "int", nullable: true),
                    MaterialId = table.Column<int>(type: "int", nullable: true),
                    PorcentajeGanancia = table.Column<double>(type: "float", nullable: true),
                    Precio = table.Column<decimal>(type: "decimal(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialEquipoAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.MaterialEquipoAreas_dbo.EquiposAreas_EquiposAreaId",
                        column: x => x.EquiposAreaId,
                        principalTable: "EquiposAreas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.MaterialEquipoAreas_dbo.Materiales_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionId",
                table: "Areas",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_AreaId",
                table: "CostosHumanosDetalle",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_CostoHumanoId",
                table: "CostosHumanosDetalle",
                column: "CostoHumanoId");

            migrationBuilder.CreateIndex(
                name: "IX_CotizacionId",
                table: "CostosHumanosDetalle",
                column: "CotizacionId");

            migrationBuilder.CreateIndex(
                name: "IX_AreaId",
                table: "EquiposAreas",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoId",
                table: "EquiposAreas",
                column: "EquipoId");

            migrationBuilder.CreateIndex(
                name: "IX_EquiposAreaId",
                table: "MaterialEquipoAreas",
                column: "EquiposAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialId",
                table: "MaterialEquipoAreas",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Cotizaciones_dbo.Clientes_ClienteId",
                table: "Cotizaciones",
                column: "ClienteId",
                principalTable: "Clientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Equipos_dbo.Gases_GasId",
                table: "Equipos",
                column: "GasId",
                principalTable: "Gases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Equipos_dbo.Marcas_MarcaId",
                table: "Equipos",
                column: "MarcaId",
                principalTable: "Marcas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Equipos_dbo.Tecnologias_TecnologiaId",
                table: "Equipos",
                column: "TecnologiaId",
                principalTable: "Tecnologias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Equipos_dbo.Tipos_TipoId",
                table: "Equipos",
                column: "TipoId",
                principalTable: "Tipos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Materiales_dbo.UnidadMedidas_UnidadMedidaId",
                table: "Materiales",
                column: "UnidadMedidaId",
                principalTable: "UnidadMedidas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
