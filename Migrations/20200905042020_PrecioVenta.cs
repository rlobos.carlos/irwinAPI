﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class PrecioVenta : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "PrecioVenta",
                table: "CotizacionEquipos",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrecioVenta",
                table: "CotizacionEquipos");
        }
    }
}
