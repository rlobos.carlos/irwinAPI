﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class ModificacionManoDeObra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Detalle",
                table: "ManoDeObra");

            migrationBuilder.DropColumn(
                name: "ProyectoId",
                table: "ManoDeObra");

            migrationBuilder.AddColumn<string>(
                name: "Codigo",
                table: "ManoDeObra",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "ManoDeObra",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Codigo",
                table: "ManoDeObra");

            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "ManoDeObra");

            migrationBuilder.AddColumn<string>(
                name: "Detalle",
                table: "ManoDeObra",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProyectoId",
                table: "ManoDeObra",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
