﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class ModificacionManoDeObra2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Costo",
                table: "ManoDeObra");

            migrationBuilder.AddColumn<double>(
                name: "Precio",
                table: "ManoDeObra",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Precio",
                table: "ManoDeObra");

            migrationBuilder.AddColumn<double>(
                name: "Costo",
                table: "ManoDeObra",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
