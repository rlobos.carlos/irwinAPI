﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace irwinAPI.Migrations
{
    public partial class Unique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PrecioMaterialProveedor_MaterialId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.CreateIndex(
                name: "IX_PrecioMaterialProveedor_MaterialId_ProveedorId",
                table: "PrecioMaterialProveedor",
                columns: new[] { "MaterialId", "ProveedorId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PrecioMaterialProveedor_MaterialId_ProveedorId",
                table: "PrecioMaterialProveedor");

            migrationBuilder.CreateIndex(
                name: "IX_PrecioMaterialProveedor_MaterialId",
                table: "PrecioMaterialProveedor",
                column: "MaterialId");
        }
    }
}
