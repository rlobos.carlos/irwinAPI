using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace IrwinAPI.Models
{
    public class User
    {
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
        public string Rol { get; set; }
    }
}