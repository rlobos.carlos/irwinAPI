using AutoMapper;
using IrwinAPI.Models.DB;
using IrwinAPI.Models.Requests;

public class AutoMapping : Profile
{
    public AutoMapping()
    {
       CreateMap<CotizacionEquipoRequest,CotizacionEquipos>();
       CreateMap<CotizacionMaterialRequest,CotizacionMateriales>();
       CreateMap<CotizacionManoDeObraRequest,CotizacionManoDeObra>();
    }
}