﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace irwinAPI.Models.DB
{
    public partial class Clientes
    {

        [Key]
        public int Id { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        [StringLength(200)]
        public string Direccion { get; set; }
        [StringLength(45)]
        public string Telefono { get; set; }
        public virtual ICollection<Cotizaciones> Cotizaciones { get; set; }
    }
}
