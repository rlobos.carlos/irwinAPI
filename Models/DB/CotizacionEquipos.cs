using System.ComponentModel.DataAnnotations.Schema;
using irwinAPI.Models.DB;

namespace IrwinAPI.Models.DB{
   public partial class CotizacionEquipos{
        public int Id{get;set;}
        public int EquipoId {get;set;}
        public int CotizacionId{get;set;}

        public double PrecioVenta {get;set;}

        [ForeignKey(nameof(EquipoId))]
        public virtual Equipos Equipos{get;set;}
        [ForeignKey(nameof(CotizacionId))]
        public virtual Cotizaciones Cotizaciones {get;set;}

    }
}