using System.ComponentModel.DataAnnotations.Schema;
using irwinAPI.Models.DB;

namespace IrwinAPI.Models.DB{
    public partial class CotizacionManoDeObra
    {
        public int Id{get;set;}
        public int ManoDeObraId {get;set;}
        public int CotizacionId{get;set;}

        [ForeignKey(nameof(ManoDeObraId))]
        public virtual ManoDeObra ManoDeObra{get;set;}
        [ForeignKey(nameof(CotizacionId))]
        public virtual Cotizaciones Cotizaciones {get;set;}

    }
}