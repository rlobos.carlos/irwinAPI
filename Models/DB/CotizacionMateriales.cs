using System.ComponentModel.DataAnnotations.Schema;
using irwinAPI.Models.DB;

namespace IrwinAPI.Models.DB{
    public partial class CotizacionMateriales{
        public int Id{get;set;}
        public int MaterialId {get;set;}
        public int CotizacionId{get;set;}

        public double PrecioVenta {get;set;}

        [ForeignKey(nameof(MaterialId))]
        public virtual Materiales Materiales{get;set;}
        [ForeignKey(nameof(CotizacionId))]
        public virtual Cotizaciones Cotizaciones {get;set;}

    }
}