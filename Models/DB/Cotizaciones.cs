﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IrwinAPI.Models.DB;

namespace irwinAPI.Models.DB
{
    public partial class Cotizaciones
    {
        
        public int Id { get; set; }

        [StringLength(2000)]
        public string Descripcion { get; set; }
        public int ClienteId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Clientes Cliente { get; set; }

        public virtual ICollection<CotizacionManoDeObra> ManoDeObra { get; set; }
        public virtual ICollection<CotizacionEquipos> Equipos { get; set; }
        public virtual ICollection<CotizacionMateriales> Materiales { get; set; }
    }
}
