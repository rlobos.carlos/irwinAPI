﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace irwinAPI.Models.DB
{
    public partial class Equipos
    {
        
        [Key]
        public int Id { get; set; }
        [StringLength(45)]
        public string Codigo { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Precio { get; set; }
        [StringLength(45)]
        public string Capacidad { get; set; }
        public int? TipoId { get; set; }
        public int? MarcaId { get; set; }
        public int? GasId { get; set; }
        public int? TecnologiaId { get; set; }
        public int? VoltajeId {get;set;}

        [ForeignKey(nameof(VoltajeId))]
        [InverseProperty(nameof(Voltajes.Equipos))]
        public virtual Voltajes Voltaje {get;set;}

    
        [ForeignKey(nameof(GasId))]
        [InverseProperty(nameof(Gases.Equipos))]
        public virtual Gases Gas { get; set; }

        [ForeignKey(nameof(MarcaId))]
        [InverseProperty(nameof(Marcas.Equipos))]
        public virtual Marcas Marca { get; set; }
        [ForeignKey(nameof(TecnologiaId))]
        [InverseProperty(nameof(Tecnologias.Equipos))]
        public virtual Tecnologias Tecnologia { get; set; }
        [ForeignKey(nameof(TipoId))]
        [InverseProperty(nameof(Tipos.Equipos))]
        public virtual Tipos Tipo { get; set; }
        
    }
}
