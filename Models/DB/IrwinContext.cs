﻿using System;
using IrwinAPI.Models;
using IrwinAPI.Models.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace irwinAPI.Models.DB
{
    public partial class IrwinContext : DbContext
    {
        public IrwinContext()
        {
        }

        public IrwinContext(DbContextOptions<IrwinContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Clientes> Clientes { get; set; }
        public virtual DbSet<Cotizaciones> Cotizaciones { get; set; }
        public virtual DbSet<Equipos> Equipos { get; set; }
        public virtual DbSet<Gases> Gases { get; set; }
        public virtual DbSet<Marcas> Marcas { get; set; }
        public virtual DbSet<Materiales> Materiales { get; set; }
        public virtual DbSet<Tecnologias> Tecnologias { get; set; }
        public virtual DbSet<Tipos> Tipos { get; set; }
        public virtual DbSet<UnidadMedidas> UnidadMedidas { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Voltajes> Voltajes { get; set; }
        public virtual DbSet<Proveedores> Proveedores{get;set;}
        public virtual DbSet<PrecioMaterialProveedor> PrecioMaterialProveedor {get;set;}
        public virtual DbSet<ManoDeObra> ManoDeObra {get;set;}
        public virtual DbSet<CotizacionEquipos> CotizacionEquipos {get;set;}
        public virtual DbSet<CotizacionMateriales> CotizacionMateriales {get;set;}
        public virtual DbSet<CotizacionManoDeObra> CotizacionManoDeObra {get;set;}

    
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PrecioMaterialProveedor>()
            .HasIndex(p => new {p.MaterialId , p.ProveedorId}).IsUnique();
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
