namespace IrwinAPI.Models
{
    public class ManoDeObra{

        public int Id {get;set;}
        public string Codigo  {get;set;}
        public string Descripcion {get;set;}
        public double Precio{get;set;}

    }
}