﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IrwinAPI.Models.DB;

namespace irwinAPI.Models.DB
{
    public partial class Materiales
    {

        [Key]
        public int Id { get; set; }
        [StringLength(45)]
        public string Codigo { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        public int? UnidadMedidaId { get; set; }
        
        [ForeignKey(nameof(UnidadMedidaId))]
        public virtual UnidadMedidas UnidadMedida { get; set; }
        public virtual ICollection<PrecioMaterialProveedor> PrecioMaterialProveedor {get;set;}
    }
}
