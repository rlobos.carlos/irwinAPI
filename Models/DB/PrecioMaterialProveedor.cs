using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using irwinAPI.Models.DB;

namespace IrwinAPI.Models.DB{
    public partial class PrecioMaterialProveedor{
        public int Id {get;set;}

        public int MaterialId {get;set;}

        public int ProveedorId {get;set;}

        public double Precio{get;set;}

        [MaxLength(100)]
        public string CodigoMaterial{get;set;}


        [ForeignKey("ProveedorId")]
        [InverseProperty(nameof(Proveedores.PrecioMaterialProveedor))]
        public Proveedores Proveedor {get;set;}

        [ForeignKey("MaterialId")]
        public Materiales Materiales{get;set;}




    }
}