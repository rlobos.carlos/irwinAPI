using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IrwinAPI.Models.DB{
    public partial class Proveedores{
        public int Id{get;set;}
        public string Nombre {get;set;}


        [InverseProperty("Proveedor")]
        public virtual ICollection<PrecioMaterialProveedor> PrecioMaterialProveedor {get;set;}

    }
}