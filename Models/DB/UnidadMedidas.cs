﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace irwinAPI.Models.DB
{
    public partial class UnidadMedidas
    {
        public UnidadMedidas()
        {
            Materiales = new HashSet<Materiales>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }
        [StringLength(45)]
        public string Abreviatura { get; set; }

        [InverseProperty("UnidadMedida")]
        public virtual ICollection<Materiales> Materiales { get; set; }
    }
}
