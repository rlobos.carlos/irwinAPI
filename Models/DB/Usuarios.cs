﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace irwinAPI.Models.DB
{
    public partial class Usuarios
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Usuario { get; set; }
        [Required]
        [StringLength(200)]
        public string Contraseña { get; set; }
    }
}
