using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace irwinAPI.Models.DB
{
    public partial class Voltajes
    {
        public Voltajes()
        {
            Equipos = new HashSet<Equipos>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(45)]
        public string Nombre { get; set; }

        [InverseProperty("Voltaje")]
        public virtual ICollection<Equipos> Equipos { get; set; }
        
    }
}
