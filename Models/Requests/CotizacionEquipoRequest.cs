namespace IrwinAPI.Models.Requests{
    public class CotizacionEquipoRequest{

        public int Id {get;set;}
        public int EquipoId{get;set;}
        public int CotizacionId{get;set;}
        public double PrecioVenta{get;set;}

    }
}