namespace IrwinAPI.Models.Requests{
    public class CotizacionManoDeObraRequest{

        public int Id {get;set;}
        public int ManoDeObraId{get;set;}
        public int CotizacionId{get;set;}
        public double Precio{get;set;}

    }
}