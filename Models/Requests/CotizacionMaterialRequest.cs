namespace IrwinAPI.Models.Requests{
    public class CotizacionMaterialRequest{

        public int Id {get;set;}
        public int MaterialId{get;set;}
        public int CotizacionId{get;set;}
        public double PrecioVenta{get;set;}

    }
}